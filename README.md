# README

## テンプレートの使い方
- TeX Liveのインストール
    - [TeX Wikiのページ](https://texwiki.texjp.org/?TeX%E5%85%A5%E6%89%8B%E6%B3%95) を参考にTeXをインストールする．
    - コマンドプロンプト/端末 で`tex --version`と打つと，`TeX 3.14159265 (TeX Live ....)`と出てくるが，....が2020以上ならOK．
- VS Codeの準備
    - VS Codeをインストールする
    - [パッケージ (LaTeX Workshop)](https://marketplace.visualstudio.com/items?itemName=James-Yu.latex-workshop) をインストールする
        - Ctrl+Shit+P (Win/Linux) Command+Shift+P (Mac) で窓が出てくるので，窓に"open setting json"を入力し"Preferences: Open Settings (JSON)"を選択．
        - 開いたファイルに以下のようにlatex-workshopの設定を入力して保存する．

```json
{
    // 他の設定が書いてある

    "latex-workshop.latex.tools": [
      {
        "name": "latexmk",
        "command": "latexmk",
        "args": [
          "%DOC%"
        ]
      },
    ],
}
```


- コンパイルのテスト
    - 以下をコマンドプロンプト/端末 で実行する
        - `git clone https://f-ttok@bitbucket.org/f-ttok/latex-thesis.git`
        - `cd latex-thesis`
        - `latexmk main.tex`
    - VS Codeを用いたコンパイル
        - `main.tex`を開く
        - Ctrl+Alt+B (Win/Linux) Command+Option+B (Mac)でコンパイル．
